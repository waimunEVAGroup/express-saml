var express = require('express');
var checkLogin = require('connect-ensure-login');
var passport = require('passport');

var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', {
    title: 'Express'
  });
});

router.post('/login',
  passport.authenticate('local', {
    failureRedirect: '/login'
  }),
  function (req, res, next) {
    res.redirect('/profile');
  });

router.get('/login', function (req, res, next) {
  res.render('login');
});

router.get('/logout', function (req, res, next) {
  req.logout();
  res.redirect('/');
});

router.get('/profile',
  checkLogin.ensureLoggedIn('/login'),
  function (req, res, next) {
    res.render('profile', {
      user: req.user
    });
  });

module.exports = router;