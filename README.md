# Express SAML

This project was generated with [Express Generator](https://expressjs.com/en/starter/generator.html) with [Passport-SAML](https://github.com/bergie/passport-saml) package, using [Gerard Braad example](https://github.com/bergie/passport-saml).

## Instructions

To install this example on your computer, clone the repository and install
dependencies.

```bash
$ git clone 
$ cd 
$ npm install
```

Start the server.

```bash
$ node ./bin/www
```

Open a web browser and navigate to [http://localhost:3000/](http://127.0.0.1:3000/)

## Switch to SAML

1. Modify the `auth/saml/config.js` file.
2. Commit `require('./auth/local/passport')(passport); in app.js`
3. Uncomment `require('./auth/local/passport')(passport);`

Modify the return done function in `auth/saml/passport.js` accordingly to return the profile of the user.

## Changes to Angular for SAML

Using [angular-saml-client](https://github.com/slem1/saml-angular) as a reference, modify the code to check for token first. Note that the example for Angular 2 and NOT AngularJS.